package com.felord.feign.api;

import com.felord.entity.User;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * Author: Dax
 * Description:
 * Date: 2017/12/17
 * Time: 20:59
 */

@Component
public class UserServiceHystrix implements UserService {
    @Override
    public User get(User user) {
        System.out.println("<><><><><><><><><><> MEMBER-SERVICE 挂了<><><><><><><><><><> ");
         user.setAge(20017);
         user.setGender("male");
         user.setName("服务挂了");
        return user;
    }
}
