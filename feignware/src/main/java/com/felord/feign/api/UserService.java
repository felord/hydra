package com.felord.feign.api;

import com.felord.entity.User;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Created with IntelliJ IDEA.
 * Author: Dax
 * Description:
 * Date: 2017/12/16
 * Time: 12:00
 */

@FeignClient(value = "MEMBER-SERVICE",fallback = UserServiceHystrix.class)
public interface UserService {
      @PostMapping("/msg/get")
     User  get(@RequestBody User user);




}
