package com.felord.sms.controller;

import com.felord.entity.User;
import com.felord.feign.api.UserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * Author: Dax
 * Description:
 * Date: 2017/12/16
 * Time: 12:10
 */

@RestController
@RequestMapping("/sms")
public class RemoteController {
  @Resource
    private UserService userService;
  @PostMapping("/go")
  public User test(User user){
      System.out.println(">>>>>>>>>>>");
      User result=userService.get(user);
      System.out.println(result);
      return result;
  }

}
