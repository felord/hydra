package com.felord.member;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

@SpringCloudApplication
@EnableFeignClients(basePackages = {"com.felord.feign"})
public class MemberServicesApplication {

    public static void main(String[] args) {
        SpringApplication.run(MemberServicesApplication.class, args);
    }
}
