package com.felord.entity;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * Author: Dax
 * Description:
 * Date: 2017/12/16
 * Time: 11:53
 */


public class User implements Serializable {

    private static final long serialVersionUID = 4333911079361590611L;
    private String name;
    private int age;
    private String gender;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    @Override
    public String toString() {
        return "{" +
                "name:" + name +
                ", age:" + age +
                ", gender:" + gender +
                '}';
    }
}
